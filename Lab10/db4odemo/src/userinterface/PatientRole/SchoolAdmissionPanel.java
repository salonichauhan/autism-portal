/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.PatientRole;

import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.PaymentRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 *
 */
public class SchoolAdmissionPanel extends javax.swing.JPanel {

    private final JPanel userProcessContainer;
    private final ArrayList<Network> networkList;
    private final UserAccount account;

    /**
     * Creates new form SchoolAdmissionRequest
     */
    public SchoolAdmissionPanel(JPanel userProcessContainer, ArrayList<Network> networkList, UserAccount account) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.networkList = networkList;
        this.account = account;
        populateOrganizationComboBox();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        schoolListTable = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        organizationJComboBox = new javax.swing.JComboBox<>();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 180), 2), "Select School", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 36), new java.awt.Color(102, 102, 180))); // NOI18N

        schoolListTable.setBackground(new java.awt.Color(204, 204, 230));
        schoolListTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "School", "Type", "Network"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(schoolListTable);

        jButton1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jButton1.setText("Request Admission");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        organizationJComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        organizationJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                organizationJComboBoxActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jButton2.setText("<<Back");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel1.setText("Type of School:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(115, 115, 115)
                        .addComponent(jButton2)
                        .addGap(67, 67, 67)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(98, 98, 98)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(59, 59, 59)
                                .addComponent(organizationJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(108, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(82, 82, 82)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(organizationJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(55, 55, 55)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap(121, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        String str = "";
        int selectedRow = schoolListTable.getSelectedRow();
        if (selectedRow < 0) {
            return;
        }
        Enterprise enterprise = (Enterprise) schoolListTable.getValueAt(selectedRow, 1);
        Organization organization = (Organization) schoolListTable.getValueAt(selectedRow, 0);
        Network network = (Network) schoolListTable.getValueAt(selectedRow, 2);
        boolean decision = organization.getName().equals(Organization.OrganizationType.PublicSchool.getValue());
        if (decision) {
            str = "Self";
        } else {
            String[] list = {"Self", "Donor"};
            JComboBox payTypeCombo = new JComboBox();
            payTypeCombo.addItem(list[0]);
            payTypeCombo.addItem(list[1]);
            //payTypeCombo.setEditable(true);
            int selectionButton = JOptionPane.OK_CANCEL_OPTION;
            selectionButton = JOptionPane.showConfirmDialog(null, payTypeCombo, "Payment Arrangement", selectionButton);
            System.out.println(selectionButton + " " + JOptionPane.OK_OPTION + " " + JOptionPane.CANCEL_OPTION);
            if (selectionButton == JOptionPane.CANCEL_OPTION) {
                return;
            }
            str = (String) payTypeCombo.getSelectedItem();
            //System.out.println(account);
            if (str.equals("Donor") && Integer.parseInt(account.getUserValue()) >= 5000) {
                JOptionPane.showMessageDialog(null, "Income should be less than 5000", "Error", JOptionPane.OK_OPTION);
                return;
            }
        }

        WorkRequest request = new WorkRequest();
        request.setPatientEmployee(account.getEmployee());
        request.setRequestType("Education");
        request.setStatus("Waiting");
        request.setDestEmployee(null);
        if (decision) {
            request.setOrganizationType(Organization.OrganizationType.PublicSchool);
        } else {
            request.setOrganizationType(Organization.OrganizationType.PrivateSchool);
        }
        request.setDestEnterprise(enterprise);
        if (str.equals("Self")) {
            request.setPaymentRequest(null);
        } else {
            request.setPaymentRequest(new PaymentRequest());
        }

        network.getWorkQueueDirectory().getWorkRequestList().add(request);
        //network.getSchoolAssignmentDirectory().getWorkRequestList().add(request);
        account.getSaQueue().getWorkRequestList().add(request);
        JOptionPane.showMessageDialog(null, "Request Successful", "Success", JOptionPane.OK_OPTION);
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);

    }//GEN-LAST:event_jButton1ActionPerformed

    private void organizationJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_organizationJComboBoxActionPerformed
        String type = (String) organizationJComboBox.getSelectedItem();
        if (type != null) {
            populateTable(type);
        }
    }//GEN-LAST:event_organizationJComboBoxActionPerformed

     private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
     }//GEN-LAST:event_jButton2ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> organizationJComboBox;
    private javax.swing.JTable schoolListTable;
    // End of variables declaration//GEN-END:variables

    private void populateTable(String type) {
        DefaultTableModel model = (DefaultTableModel) schoolListTable.getModel();

        model.setRowCount(0);
        for (Network network : networkList) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                if (enterprise.getEnterpriseType() == Enterprise.EnterpriseType.Education) {
                    for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                        if (organization.getName().equals(type)) {
                            Object[] row = new Object[3];
                            row[1] = enterprise;
                            row[0] = organization;
                            row[2] = network;
                            model.addRow(row);
                        }
                    }
                }
            }
        }
    }

    public void populateOrganizationComboBox() {
        organizationJComboBox.removeAllItems();
        organizationJComboBox.addItem(Organization.OrganizationType.PublicSchool.getValue());
        organizationJComboBox.addItem(Organization.OrganizationType.PrivateSchool.getValue());
        populateTable((String) organizationJComboBox.getSelectedItem());
    }

}
