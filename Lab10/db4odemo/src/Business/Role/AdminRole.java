/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userinterface.AdministrativeRole.AdminWorkAreaJPanel;
import userinterface.PatientRole.PatientWorkAreaJPanel;

public class AdminRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business, Network network) {
        if (enterprise.getEnterpriseType() == Enterprise.EnterpriseType.Patient) {
            return new PatientWorkAreaJPanel(userProcessContainer, business, enterprise, account);
        }
        return new AdminWorkAreaJPanel(userProcessContainer, enterprise);
    }

}

