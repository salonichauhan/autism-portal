/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Network;

import Business.Enterprise.EnterpriseDirectory;
import Business.WorkQueue.WorkQueue;

/**
 *
 * @author MyPC1
 */
public class Network {
    private String name;
    private EnterpriseDirectory enterpriseDirectory;
    private WorkQueue workQueueDirectory;
    
    public Network(){
         enterpriseDirectory=new EnterpriseDirectory();
        workQueueDirectory = new WorkQueue();
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EnterpriseDirectory getEnterpriseDirectory() {
        return enterpriseDirectory;
    }
    
    @Override
    public String toString(){
        return name;
    }

    public WorkQueue getWorkQueueDirectory() {
        return workQueueDirectory;
    }

    public void setWorkQueueDirectory(WorkQueue workQueueDirectory) {
        this.workQueueDirectory = workQueueDirectory;
    }
    
}
