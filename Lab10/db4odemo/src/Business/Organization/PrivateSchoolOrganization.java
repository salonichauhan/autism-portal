/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.PrivateSchoolRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Saloni
 */
public class PrivateSchoolOrganization extends Organization{
    
    public PrivateSchoolOrganization() {
        super(OrganizationType.PrivateSchool.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new PrivateSchoolRole());
        return roles;
    }
}
