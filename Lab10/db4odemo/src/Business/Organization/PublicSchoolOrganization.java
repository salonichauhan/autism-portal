/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.PublicSchoolRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Saloni
 */
public class PublicSchoolOrganization extends Organization{
    
    public PublicSchoolOrganization() {
        super(Organization.OrganizationType.PublicSchool.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new PublicSchoolRole());
        return roles;
    }
}
