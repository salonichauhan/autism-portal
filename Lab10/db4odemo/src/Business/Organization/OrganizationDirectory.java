/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.OrganizationType;
import java.util.ArrayList;


public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(OrganizationType type){
       Organization organization = null;
        if (null != type) switch (type) {
            case Doctor:
                organization = new DoctorOrganization();
                organizationList.add(organization);
                System.err.println(organizationList);
                break;
            case CareTaker:
                organization = new CareTakerOrganization();
                organizationList.add(organization);
                break;
            case Donor:
                organization = new DonorOrganization();
                organizationList.add(organization);
                break;
            case PublicSchool:
                organization = new PublicSchoolOrganization();
                organizationList.add(organization);
                break;
            case PrivateSchool:
                organization = new PrivateSchoolOrganization();
                organizationList.add(organization);
                break;
            default:
                break;
        }
        return organization;
    }
}