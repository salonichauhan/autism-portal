/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Employee.EmployeeDirectory;
import Business.Enterprise.Enterprise;
import Business.Role.Role;
import Business.UserAccount.UserAccountDirectory;
import java.util.ArrayList;


public abstract class Organization {

     private String name;
    
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;
    private int organizationID;
    public static int counter = 0;

    public enum OrganizationType {
        Admin("Admin Organization", null),
        Doctor("Doctor Organization", Enterprise.EnterpriseType.Hospital),
        CareTaker("Caretaker Organization", Enterprise.EnterpriseType.Hospital),
        Donor("Donor Organization", Enterprise.EnterpriseType.NGO),
        PublicSchool("Public School Organization", Enterprise.EnterpriseType.Education),
        PrivateSchool("Private School Organization", Enterprise.EnterpriseType.Education);
        private String value;
        private Enterprise.EnterpriseType enterpriseType;

        private OrganizationType(String value, Enterprise.EnterpriseType enterpriseType) {
            this.value = value;
            this.enterpriseType = enterpriseType;
        }

        public String getValue() {
            return value;
        }

        public Enterprise.EnterpriseType getEType() {
            return enterpriseType;
        }

        @Override
        public String toString() {
            return value;
        }

    }

    public Organization(String name) {
        this.name = name;
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
        organizationID = counter++;
    }

    public abstract ArrayList<Role> getSupportedRole();

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}