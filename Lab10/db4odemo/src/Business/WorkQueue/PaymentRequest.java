/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;
import Business.Employee.Employee;
import Business.Enterprise.Enterprise;

/**
 *
 * @author agarw
 */
public class PaymentRequest {
    private Enterprise schoolEnterprise;
    private Enterprise ngoEnterprise;
    private Employee donorEmployee;

    public PaymentRequest() {
        schoolEnterprise = null;
        donorEmployee = null;
        ngoEnterprise = null;

    }

    public Enterprise getSchoolEnterprise() {
        return schoolEnterprise;
    }

    public void setSchoolEnterprise(Enterprise schoolEnterprise) {
        this.schoolEnterprise = schoolEnterprise;
    }

    public Enterprise getNgoEnterprise() {
        return ngoEnterprise;
    }

    public void setNgoEnterprise(Enterprise ngoEnterprise) {
        this.ngoEnterprise = ngoEnterprise;
    }

    public Employee getDonorEmployee() {
        return donorEmployee;
    }

    public void setDonorEmployee(Employee donorEmployee) {
        this.donorEmployee = donorEmployee;
    }

    
}
