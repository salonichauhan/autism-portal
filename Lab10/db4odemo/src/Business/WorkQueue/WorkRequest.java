/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;
import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;

/**
 *
 * @author agarw
 */
public class WorkRequest {
    private Employee patientEmployee;
    private Employee destEmployee; //intermediate destination
    private Organization.OrganizationType organizationType;
    private Enterprise destEnterprise;
    private String requestType, status;
    private PaymentRequest paymentRequest;

    public PaymentRequest getPaymentRequest() {
        return paymentRequest;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPaymentRequest(PaymentRequest paymentRequest) {
        this.paymentRequest = paymentRequest;
    }

    public Employee getPatientEmployee() {
        return patientEmployee;
    }

    public void setPatientEmployee(Employee patientEmployee) {
        this.patientEmployee = patientEmployee;
    }

    public Employee getDestEmployee() {
        return destEmployee;
    }

    public void setDestEmployee(Employee destEmployee) {
        this.destEmployee = destEmployee;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public Enterprise getDestEnterprise() {
        return destEnterprise;
    }

    public void setDestEnterprise(Enterprise destEnterprise) {
        this.destEnterprise = destEnterprise;
    }

    public Organization.OrganizationType getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(Organization.OrganizationType organizationType) {
        this.organizationType = organizationType;
    }

    @Override
    public String toString() {
        return patientEmployee.toString();
    }
    
}
